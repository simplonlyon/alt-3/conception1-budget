
# Conception & UML partie 1 - Gestion de budget

  

## Description du projet

  

Afin de négocier une augmentation à votre travail, ou pour en trouver un nouveau, vous avez décidé de mettre à jour votre portfolio sur gitlab.

  Vous avez décidé de coder un tout nouveau projet, qui mettra en valeur l'ensemble de vos compétences.

Pour joindre l'utile à l'agréable, et afin de mieux maîtriser vos dépenses mensuelles, vous avez décidé de vous créer une petite application de gestion de budget que vous allez utiliser dans le cadre de votre foyer.

## Fonctionnalités attendues

**Fonctionnalités obligatoires :**

  * gérer un budget mensuel : entrées et sorties
  * les entrées doivent pouvoir être catégorisées (loisirs, famille, loyer/prêt immobilier, etc)
  * il doit être possible de "geler" une entrée : une entrée gelée n'est plus modifiable
  * l'application doit permettre d'éditer des statistiques mensuelles sur le pourcentage de dépenses par type, l'évolution au fil du temps, etc


*Fonctionnalités **bonus** :*

  * possibilité de gérer un budget commun, et des budgets séparés
  * possibilité pour les parents de voir le budget de leurs enfants, mais pas l'inverse
  * possibilité de faire un budget prévisionnel, c'est à dire dans le futur, pour planifier des grosses dépenses (vacances, réparation de voiture, achat immobilier, etc)
  * possibilité de voir son solde bancaire et son évolution au fil du temps
  * possibilité d'ajouter des justificatifs (tickets de caisse, ticket de péage, facture, etc)
  *  possibilité de désigner une entrée comme étant récurrente (elle est automatiquement ajoutée tous les mois, par exemple votre abonnement à Internet)

## Éléments de rendu

Faites la conception comme vous en avez l'habitude. 
Vous devez rendre : 
  * un diagramme de cas d'utilisation complet
  * un modèle de données complet (comment vous stockeriez les données en base), format libre (mcd, extrait de diagramme de classe, etc).
  * un diagramme de classe détaillé : classes, interfaces, héritages éventuels, attributs des classes avec leur visibilité, méthodes des classes avec leur visibilité
  * un fichier Readme détaillant vos choix de conception : le nom des méthodes, pourquoi telle classe, pourquoi telle visibilité, etc
  * **bonus** : un logigramme de vos méthodes "importantes"

Vos diagrammes doivent être versionnés sur un dépôt git (lors du rendu sur Simplonline, vous fournirez juste l'URL de votre dépôt, pas les diagrammes un par un). Chaque commit doit être accompagné d'un message clair expliquant les modifications par rapport au précédent.

Si votre diagramme de classe n'est pas complet (vous n'avez pas eu le temps de détailler toutes vos classes), ce n'est pas grave. Toutes les classes doivent être présentes, puis vous les détaillez une par une. Le but de ce projet est de voir votre réflexion. Indiquez simplement dans le Readme les éléments auxquels vous avez pensé mais que vous n'avez pas eu le temps d'intégrer à votre diagramme.

## Contexte de travail

Le travail est à faire INDIVIDUELLEMENT.
Le rendu est à faire sur Simplonline pour le mercredi 27 avril 2022 à 9h.
